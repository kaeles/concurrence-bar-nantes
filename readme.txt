=== Concurrence Bar Nantes ===
Contributors: vblee
Tags: dashboard, concurrence bar, nantes
Requires at least: 4.7
Tested up to: 5.4
Requires PHP: 7.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

List all bar in Nantes and these informations.
