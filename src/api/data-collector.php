<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Get all concurrence
 * @return array List of concurrence
 */
// Return all concurrence
function hdra_get_all_concurrence( $request ) {
    if (
      false === ( $all_concurrence = get_transient( 'hdra_concurrence' ) )
      || empty( get_transient( 'hdra_concurrence' ) ) ) {

      global $wpdb;
      $table_name = $wpdb->prefix . 'hdra_concurrence';

      $all_concurrence = $wpdb->get_results( "SELECT * FROM $table_name" );
      // cache for 2 hours
      set_transient( 'hdra_concurrence', $all_concurrence, 60*60*2 );
    }

    return rest_ensure_response( $all_concurrence );
}

/**
 * Set concurrence
 */
// Set concurrence and save in database
function hdra_set_concurrence( $data ) {
  $result = json_decode( $data->get_body(), true );
  global $wpdb;
  $table_name = $wpdb->prefix . 'hdra_concurrence';

  foreach ( $result as $value ) {
    if ( array_key_exists( 'siret', $value ) && $value['siret'] !== null ) {
      $opening_date = str_replace( '/', '-', $value['opening_date'] );
      $response = $wpdb->replace( $table_name, array(
        'signboard'     => $value['signboard'],
        'name'          => $value['name'],
        'siret'         => $value['siret'],
        'judicial_form' => $value['judicial_form'],
        'gps'           => $value['gps'],
        'opening_date'  => date( 'Y-m-d', strtotime( $opening_date ) ),
        'employees'     => $value['employees'],
        'surface'       => $value['surface'],
        'comment'       => $value['comment'],
        'strength'      => $value['strength']
      ) );
    }
  }
  delete_transient( 'hdra_concurrence' );
  return json_encode( $response );
}

/**
 * Remove concurrence
 */
// Remove Concurrence by ID.
function hdra_remove_concurrence( $request ) {
  global $wpdb;

  $table_name = $wpdb->prefix . 'hdra_concurrence';

  $response = $wpdb->delete(
    $table_name,
    array( 'siret' => $request->get_param( 'id' ) ),
    array( '%d' )
  );

  delete_transient( 'hdra_concurrence' );
  return json_encode( $response );
}
