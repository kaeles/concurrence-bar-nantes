<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

global $wpdb;

$table_name = $wpdb->prefix . 'hdra_concurrence';
$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE $table_name (
  siret bigint NOT NULL,
  signboard tinytext DEFAULT '',
  name tinytext DEFAULT '',
  judicial_form tinytext DEFAULT '',
  gps tinytext DEFAULT '',
  opening_date datetime DEFAULT '0000-00-00' NOT NULL,
  employees tinytext DEFAULT '0 salarié',
  surface tinytext,
  comment text,
  strength tinyint,
  PRIMARY KEY  (siret)
) $charset_collate;";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );
