<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require( 'data-collector.php' );

add_action( 'rest_api_init',function () {
  register_rest_route( 'rivals-analytics/v1', 'competings', array(
    'methods' => WP_REST_Server::READABLE,
    'callback' => 'hdra_get_all_concurrence',
  ) );
  register_rest_route( 'rivals-analytics/v1', 'competings', array(
    'methods' => WP_REST_Server::EDITABLE,
    'callback' => 'hdra_set_concurrence',
  ) );
  register_rest_route( 'rivals-analytics/v1', 'competings/(?P<id>[\d]+)', array(
    'methods' => WP_REST_Server::DELETABLE,
    'callback' => 'hdra_remove_concurrence',
  ) );
} );
