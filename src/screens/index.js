const { render } = wp.element;

import DataCollector from './../components/data-collector/index.js';
import Dashboard from './../components/dashboard/index.js';

document.addEventListener( 'DOMContentLoaded', function() {
	if ( document.getElementById( 'hdra-data-collector' ) !== null ) {
		render(
			<DataCollector />,
			document.getElementById( 'hdra-data-collector' )
		);
	}
	if ( document.getElementById( 'hdra-rivals-dashboard' ) !== null ) {
		render(
			<Dashboard />,
			document.getElementById( 'hdra-rivals-dashboard' )
		);
	}
} );
