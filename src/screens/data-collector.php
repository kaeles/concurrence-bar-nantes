<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function hdra_data_collector_menu_page() {
  add_submenu_page(
    'options-general.php',
    'Data Collector',
    'Rivals Analytics',
    'manage_options',
    'data-collector.php',
    'hdra_data_collector_page_html'
   );
}
add_action( 'admin_menu', 'hdra_data_collector_menu_page' );

function hdra_data_collector_page_html() {
  echo '<div id="hdra-data-collector"></div>';
}
