<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function hdra_dashboard_menu_page() {
  add_menu_page(
    'Dashboard',
    'Rivals Analytics',
    'edit_posts',
    'rivals-dashboard',
    'hdra_dashboard_page_html',
    'dashicons-analytics',
    2
   );
}
add_action( 'admin_menu', 'hdra_dashboard_menu_page' );

function hdra_dashboard_page_html() {
  echo '<div id="hdra-rivals-dashboard"></div>';
}
