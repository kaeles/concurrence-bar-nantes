const { Component, render } = wp.element;

import Papa from 'papaparse';
import './style.css';

import DataTable from './../data-table/index.js';

export default class DataCollector extends Component {
	constructor( props ) {
		super( props );

		props.apiUrl = '/wp-json/rivals-analytics/v1/competings';
		props.container = document.getElementById( 'data-collected' );

		this.state = {
			file: [],
			data: [],
			error: '',
		};

		this.getData = this.getData.bind( this );
		this.postData = this.postData.bind( this );
		this.removeData = this.removeData.bind( this );
		this.onChangeHandler = this.onChangeHandler.bind( this );
		this.onClickHandler = this.onClickHandler.bind( this );
	}

	render() {
		return (
			<div className="wrap">
				<h1>Rivals Analytics Settings</h1>
				<div className="data-collector">
					<h2>Data collector</h2>
					<p>
						Please, select csv file to import data about near
						concurrence.
					</p>
					<p className="actions bulkactions">
						<input
							type="file"
							name="file"
							accept=".csv"
							onChange={ this.onChangeHandler }
							className="browser button button-hero"
							style={ { 'margin-right': '6px' } }
						/>
						<button
							type="button"
							className="button button-primary"
							onClick={ this.onClickHandler }
						>
							Import
						</button>
					</p>
					<div id="data-table" />
				</div>
			</div>
		);
	}

	componentDidMount() {
		this.getConcurrence();
	}

	componentWillUnmount() {}

	componentDidUpdate() {}

	onChangeHandler( event ) {
		const file = event.target.files[ 0 ];
		this.setState( {
			file,
		} );
	}

	onClickHandler() {
		this.parseCSV();
	}

	parseCSV() {
		const self = this;
		Papa.parse( this.state.file, {
			header: true,
			dynamicTyping: true,
			newline: true,
			complete( results ) {
				self.setConcurrence( results.data );
				self.postData();
			},
		} );
	}

	getConcurrence() {
		this.getData();
		return this.state.data;
	}

	setConcurrence( json ) {
		const data = JSON.stringify( json );
		this.setState( { data } );
	}

	getData() {
		const options = { method: 'GET' };
		window
			.fetch( this.props.apiUrl, options )
			.then(
				( response ) => response.json(),
				( error ) => {
					this.setState( {
						error,
					} );
				}
			)
			.then( ( results ) => {
				if ( ! results.code ) {
					const data = results;
					this.setState( {
						data,
					} );
					render(
						<DataTable
							data={ this.state.data }
							postData={ this.postData }
							removeData={ this.removeData }
						/>,
						document.getElementById( 'data-table' )
					);
				}
				this.setState( {
					error: results.message,
				} );
			} );
	}

	postData( data ) {
		const myHeaders = new window.Headers();
		myHeaders.append( 'Content-Type', 'application/json' );
		const options = {
			method: 'POST',
			body: JSON.stringify( data ),
		};
		window.fetch( this.props.apiUrl, options ).then(
			( response ) => {
				response.json();
			},
			( error ) =>
				this.setState( {
					status: error,
				} )
		);
	}

	removeData( data ) {
		const myHeaders = new window.Headers();
		myHeaders.append( 'Content-Type', 'application/json' );
		const options = {
			method: 'DELETE',
		};
		window.fetch( this.props.apiUrl + '/' + data.siret, options ).then(
			( response ) => {
				response.json();
			},
			( error ) => {
				this.setState( {
					status: error,
				} );
			}
		);
	}
}
