const { Component, render } = wp.element;

import $ from 'jquery';

import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { Icon } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import './style.css';

export const icon = new Icon( {
	iconUrl: `https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png`,
	iconSize: [ 25, 41 ],
} );

export default class DataMap extends Component {
	constructor( props ) {
		super( props );
		props.apiUrl = '/wp-json/rivals-analytics/v1/competings';
		this.state = {
			data: [],
			tiles: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
			tilesAttr:
				'&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
			position: [ '47.2383172', '-1.5226357' ],
			markers: [],
			zoomLevel: 12,
		};
	}

	render() {
		return (
			<div className="wrap">
				<h1>Rivals Analytics</h1>
				<div className="data-map">
					<h2>Data map</h2>
					<div>
						<Map
							center={ this.state.position }
							zoom={ this.state.zoomLevel }
						>
							<TileLayer
								attribution={ this.state.tilesAttr }
								url={ this.state.tiles }
							/>
							{ this.state.data.map( ( data, key ) => {
								if (
									data &&
									typeof data.gps !== 'undefined' &&
									data.gps !== null &&
									data.gps !== '' &&
									data.gps.indexOf( ',' ) > -1
								) {
									return (
										<Marker
											key={ key }
											position={ data.gps
												.replace( ' ', '' )
												.split( ',' ) }
											icon={ icon }
										>
											<Popup>
												<table className="marker-properties">
													<tbody>
														<tr>
															<th>Siret</th>
															<td>
																{ data.siret }
															</td>
														</tr>
														<tr>
															<th>Name</th>
															<td>
																{ data.name }
															</td>
														</tr>
														<tr>
															<th>Enseigne</th>
															<td>
																{
																	data.signboard
																}
															</td>
														</tr>
														<tr>
															<th>
																Forme juridique
															</th>
															<td>
																{
																	data.judicial_form
																}
															</td>
														</tr>
														<tr>
															<th>
																Date
																d&apos;ouverture
															</th>
															<td>
																{
																	data.opning_date
																}
															</td>
														</tr>
														<tr>
															<th>Employés</th>
															<td>
																{
																	data.employees
																}
															</td>
														</tr>
														<tr>
															<th>Surface</th>
															<td>
																{ data.surface }
															</td>
														</tr>
														<tr>
															<th>Commentaire</th>
															<td>
																{ data.comment.replace(
																	'\n',
																	'<br>'
																) }
															</td>
														</tr>
													</tbody>
												</table>
											</Popup>
										</Marker>
									);
								}

								return false;
							} ) }
						</Map>
					</div>
				</div>
			</div>
		);
	}

	componentDidMount() {}

	componentWillMount() {
		this.getData();
	}

	componentDidUpdate() {}

	getData() {
		const options = { method: 'GET' };
		window
			.fetch( this.props.apiUrl, options )
			.then( ( response ) => response.json() )
			.then( ( results ) => {
				this.setState( {
					data: results,
				} );
			} );
	}
}
