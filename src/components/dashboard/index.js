const { Component } = wp.element;

import DataMap from './../data-map/index.js';

export default class Dashboard extends Component {
	render() {
		return (
			<div className="wrap">
				<h1>Rivals Analytics</h1>
				<div className="dashboard">
					<h1>Dashboard</h1>
					<DataMap />
				</div>
			</div>
		);
	}

	componentDidMount() {}

	componentWillUnmount() {}

	componentDidUpdate() {}
}
