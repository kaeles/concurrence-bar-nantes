const { Component } = wp.element;

import $ from 'jquery';
import DataTables from 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-dt/js/dataTables.dataTables.js';
import './style.css';

$.DataTable = DataTables;

export default class DataTable extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			data: [],
			headers: [],
			siret: '',
			signboard: '',
			name: '',
			judicial_form: '',
			gps: '',
			opening_date: '',
			employees: '',
			surface: '',
			comment: '',
			strength: '',
		};
	}

	handleChange = ( event ) => {
		if ( event.target.name === 'siret' )
			this.setState( { siret: event.target.value } );
		if ( event.target.name === 'signboard' )
			this.setState( { signboard: event.target.value } );
		if ( event.target.name === 'name' )
			this.setState( { name: event.target.value } );
		if ( event.target.name === 'judicial_form' )
			this.setState( { judicial_form: event.target.value } );
		if ( event.target.name === 'gps' )
			this.setState( { gps: event.target.value } );
		if ( event.target.name === 'opening_date' )
			this.setState( { opening_date: event.target.value } );
		if ( event.target.name === 'employees' )
			this.setState( { employees: event.target.value } );
		if ( event.target.name === 'surface' )
			this.setState( { surface: event.target.value } );
		if ( event.target.name === 'comment' )
			this.setState( { comment: event.target.value } );
		if ( event.target.name === 'strength' )
			this.setState( { strength: event.target.value } );
	};

	handleUpdate = ( event ) => {
		const values = {};
		const datatable = $( '#sortable-data' ).DataTable();
		datatable
			.$( 'tr.selected' )
			.children()
			.each( function( index ) {
				const key = $( datatable.column( index ).header() )
					.html()
					.toLowerCase()
					.replace( ' ', '_' );
				const value = $( this ).html();
				values[ key ] = value;
			} );
		this.setState( values );
		$( '.modal' ).css( 'display', 'flex' );
		event.preventDefault();
	};

	handleAdd = ( event ) => {
		$( '.modal' ).css( 'display', 'flex' );
		event.preventDefault();
	};

	handleSubmit = ( event ) => {
		if ( this.state.siret !== '' ) {
			const newData = {
				siret: this.state.siret,
				signboard: this.state.signboard,
				name: this.state.name,
				judicial_form: this.state.judicial_form,
				gps: this.state.gps,
				opening_date: this.state.opening_date,
				employees: this.state.employees,
				surface: this.state.surface,
				comment: this.state.comment,
				strength: this.state.strength,
			};
			const data = this.state.data.filter( ( obj ) => {
				return obj.siret !== newData.siret ? true : false;
			} );
			this.setState(
				{
					data: [ ...data, newData ],
					siret: '',
					signboard: '',
					name: '',
					judicial_form: '',
					gps: '',
					opening_date: '',
					employees: '',
					surface: '',
					comment: '',
					strength: '',
				},
				() => {
					const datatable = $( '#sortable-data' ).DataTable();
					datatable.clear();
					datatable.rows.add( this.state.data );
					datatable.draw();
					this.props.data = this.state.data;
					this.props.postData( this.state.data );
					$( '.modal' ).css( 'display', 'none' );
				}
			);
		}
		event.preventDefault();
	};

	handleCancel = ( event ) => {
		$( '.modal' ).css( 'display', 'none' );
		this.setState( {
			siret: '',
			signboard: '',
			name: '',
			judicial_form: '',
			gps: '',
			opening_date: '',
			employees: '',
			surface: '',
			comment: '',
			strength: '',
		} );
		event.preventDefault();
	};

	handleRemove = ( event ) => {
		const table = $( '#sortable-data' ).DataTable();
		const data = table.row( '.selected' ).data();
		this.props.removeData( data );
		table
			.row( '.selected' )
			.remove()
			.draw( false );
		event.preventDefault();
	};

	render() {
		return (
			<>
				<h2>Manage your rivals list</h2>
				<button onClick={ this.handleAdd } className="button action">
					Add new record
				</button>
				<button onClick={ this.handleRemove } className="button action">
					Remove record
				</button>
				<button onClick={ this.handleUpdate } className="button action">
					Update record
				</button>
				<form id="dialog" className="modal hidden">
					{ this.state.headers.map( ( data, key ) => {
						return this.renderDataForm( data, key );
					} ) }
					<div className="controls">
						<button
							onClick={ this.handleSubmit }
							className="button action"
						>
							Submit
						</button>
						<button
							onClick={ this.handleCancel }
							className="button action close"
						>
							Cancel
						</button>
					</div>
				</form>
				<h2>Your data in database</h2>
				<form onSubmit={ this.handleUpdate }>
					<table
						id="sortable-data"
						className="table table-striped table-bordered"
					></table>
				</form>
			</>
		);
	}

	componentWillMount() {
		this.setState( { headers: this.getHeaders() } );
		this.setState( { data: this.props.data } );
	}

	componentDidMount() {
		this.renderDataTable();
	}

	renderDataTable() {
		const headers = this.state.headers;
		const table = $( '#sortable-data' ).DataTable( {
			columns: headers,
			data: this.state.data,
		} );
		$( '#sortable-data' ).on( 'click', 'tr', function() {
			if ( $( this ).hasClass( 'selected' ) ) {
				$( this ).removeClass( 'selected' );
			} else {
				table.$( 'tr.selected' ).removeClass( 'selected' );
				$( this ).addClass( 'selected' );
			}
		} );
	}

	renderDataForm( data, key ) {
		if ( data.title.toLowerCase().includes( 'comment' ) ) {
			return (
				<div key={ key }>
					<label htmlFor={ data.data }>
						<span>{ data.title } :</span>
						<textarea
							name={ data.data }
							onChange={ this.handleChange }
							value={ this.state[ data.data ] }
						/>
					</label>
					<br />
				</div>
			);
		} else if ( data.title.toLowerCase().includes( 'date' ) ) {
			return (
				<div key={ key }>
					<label htmlFor={ data.data }>
						<span>{ data.title } :</span>
						<input
							type="date"
							name={ data.data }
							onChange={ this.handleChange }
							value={ this.state[ data.data ] }
						/>
					</label>
					<br />
				</div>
			);
		} else if ( data.title.toLowerCase().includes( 'strength' ) ) {
			return (
				<div key={ key }>
					<label htmlFor={ data.data }>
						<span>{ data.title } :</span>
						<input
							type="number"
							min="1"
							max="3"
							name={ data.data }
							onChange={ this.handleChange }
							value={ this.state[ data.data ] }
						/>
					</label>
					<br />
				</div>
			);
		}
		return (
			<div key={ key }>
				<label htmlFor={ data.data }>
					<span>{ data.title } :</span>
					<input
						type="text"
						name={ data.data }
						onChange={ this.handleChange }
						value={ this.state[ data.data ] }
					/>
				</label>
				<br />
			</div>
		);
	}

	getHeaders() {
		const headers = [];
		for ( const key in Object.keys( this.props.data[ 0 ] ) ) {
			headers.push( {
				data: Object.keys( this.props.data[ 0 ] )[ key ],
				title: Object.keys( this.props.data[ 0 ] )
					[ key ].toUpperCase()
					.replace( '_', ' ' ),
			} );
		}
		return headers;
	}
}
