const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

module.exports = {
	...defaultConfig,
	module: {
		...defaultConfig.module,
		rules: [
			...defaultConfig.module.rules,
			{
				test: /\.css$/i,
				use: [
					{ loader: MiniCssExtractPlugin.loader },
					{ loader: 'css-loader' },
				],
			},
			{
				test: /.*\.(gif|png|jpe?g|svg)$/i,
				loader: 'file-loader?hash=sha512&digest=hex&name=[name].[ext]',
				options: {
					name: 'images/[name].[ext]',
				},
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin( {
			filename: '[name].css',
			chunkFilename: '[id].css',
			ignoreOrder: false,
		} ),
	],
};
