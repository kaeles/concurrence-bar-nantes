<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/**
 * Plugin Name:       Rivals analytics
 * Plugin URI:        https://hyper-drive.fr/plugins/rivals-analytics/
 * Description:       Enable to monitoring your competing companies.
 * Version:           0.0.1
 * Requires at least: 4.7
 * Requires PHP:      7.1
 * Author:            Vincent Blée
 * Author URI:        https://vincent.hyper-drie.fr/
 * License:           GPL v3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:       rivals-analytics
 * Domain Path:       /languages
 */

function hdra_init() {
  // Load the bundle APP.
  add_action( 'admin_enqueue_scripts', 'load_bundle' );
  // Registering API routes.
  require( __DIR__ . '/src/api/routes.php' );
  // Create admin pages.
  require( __DIR__ . '/src/screens/screens.php' );
}
add_action( 'init', 'hdra_init' );

function hdra_activate() {
  // Manage database to create new table.
  require( __DIR__ . '/src/api/database-table.php' );
  // Clear the permalinks after the post type has been registered.
  flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'hdra_activate' );

function load_bundle() {
  wp_enqueue_script(
    'hdra-bundle',
    plugin_dir_url( '__FILE__' ) . 'concurrence-bar-nantes/build/index.js',
    array( 'wp-element', 'jquery-ui-dialog' ),
    '0.0.1'
  );
  wp_enqueue_style( 'hdra-style', plugin_dir_url( '__FILE__' ) . 'concurrence-bar-nantes/build/index.css', null, '0.0.1' );
}
